package in.himtech.persist.ctrl;

import org.hibernate.Query;
import org.hibernate.Session;

public class QueryCacheMain {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Query query = session.createQuery("from Tourist where tourId=1");
		query.setCacheable(true);
		query.uniqueResult();
		
		session.getTransaction().commit();
		session.close();
		
		Session session1 = HibernateUtil.getSessionFactory().openSession();
		session1.beginTransaction();
		
		Query query1 = session1.createQuery("from Tourist where tourId=1");
		query1.setCacheable(true);
		query1.uniqueResult();
		
		session1.getTransaction().commit();
		session1.close();
		
		HibernateUtil.shutdown();
	}
}
