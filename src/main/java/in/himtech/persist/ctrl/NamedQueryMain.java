package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Tourist;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class NamedQueryMain {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Query query = session.getNamedQuery("touriest.all");
		List<Tourist> listTourist = query.list();
		System.out.println("Named Query All: " + listTourist);
		
		Query query1 = session.getNamedQuery("tourist.byid");
		query1.setInteger(0, 3);
		List<Tourist> listTourist1 = query1.list();
		System.out.println("Named Query > 3: " + listTourist1);
		
		Query query2 = session.getNamedQuery("tourist.native.byname");
		query1.setInteger(0, 3);
		List<Tourist> listTourist2 = query2.list();
		System.out.println("Named Native Query All: " + listTourist2);
		
		session.getTransaction().commit();
		session.close();
		HibernateUtil.shutdown();
	}
	
}
