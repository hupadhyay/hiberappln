package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Person;
import in.himtech.persist.dto.UserDetails;

import java.util.Date;

import org.hibernate.Session;

public class UserDetailMain {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.setAdharNumber("werwerwer");
		p1.setName("Hritika");
		p1.setPancard("ABKPU8765W");
		
		UserDetails ud = new UserDetails();
		ud.setPerson(p1);;
		ud.setUserName("Hritika");
		ud.setAddress("Sarojini Nagar, Lucknow");
		ud.setDescription("My Lovely Daughter");
		ud.setJoiningDate(new Date());
		
		Person p2 = new Person();
		p2.setAdharNumber("dededede");
		p2.setName("Hrishik");
		p2.setPancard("ABKPU7272T");
		
		UserDetails ud1 = new UserDetails();
		ud1.setPerson(p2);
		ud1.setUserName("Hrishik");
		ud1.setAddress("Sarojini Nagar, Lucknow");
		ud1.setDescription("My Lovely Son");
		ud1.setJoiningDate(new Date());
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(ud);
		session.save(ud1);
		session.getTransaction().commit();
		session.close();
		
		// Getting Object from database
//		session = HibernateUtil.getSessionFactory().openSession();
//		UserDetails ud1 = (UserDetails)session.get(UserDetails.class, 1);
//		System.out.println(ud1);
		HibernateUtil.shutdown();
	}
}
