package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Tourist;

import org.hibernate.Session;

public class DetachToPersistMain {
	public static void main(String[] args) {

		// Transient State
		Tourist tourist = new Tourist();
		tourist.setName("Adam Gilchrist");

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		session.save(tourist);
		
		session.getTransaction().commit();
		session.close();

		// Now session object is detached.
		tourist.setName("Barak Obama");
		
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		//Detached object now converted to persisted object.
		session.update(tourist);
		
		session.getTransaction().commit();
		session.close();
		
		HibernateUtil.shutdown();
	}
}
