package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Driver;
import in.himtech.persist.dto.Vehicle;

import org.hibernate.Session;

public class OneToOneMain {

	public static void main(String[] args) {
		
		Driver driver = new Driver();
		driver.setName("Himanshu");

		Vehicle vehicle = new Vehicle();
		vehicle.setName("Maruti Alto K10");
		
		driver.setVehicle(vehicle);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(driver);
		session.save(vehicle);
		session.getTransaction().commit();
		session.close();

		HibernateUtil.shutdown();
	}

}
