package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Book;
import in.himtech.persist.dto.Chapter;

import org.hibernate.Session;

public class ManyToOneMain {

	public static void main(String[] args) {
		
		Book book = new Book();
		book.setName("Learn JPA");
		
		Chapter cptr1 = new Chapter();
		cptr1.setTitle("Introduction");
		cptr1.setBook(book);
		
		Chapter cptr2 = new Chapter();
		cptr2.setTitle("Basic");
		cptr2.setBook(book);
		
		Chapter cptr3 = new Chapter();
		cptr3.setTitle("Advance JPA");
		cptr3.setBook(book);
		
		Chapter cptr4 = new Chapter();
		cptr4.setTitle("At Last Revise");
		cptr4.setBook(book);
		
		book.getListChapter().add(cptr1);
		book.getListChapter().add(cptr2);
		book.getListChapter().add(cptr3);
		book.getListChapter().add(cptr4);

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(book);
		session.save(cptr1);
		session.save(cptr2);
		session.save(cptr3);
		session.save(cptr4);
		session.getTransaction().commit();
		session.close();

		HibernateUtil.shutdown();
	}
}
