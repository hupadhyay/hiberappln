package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Tourist;

import org.hibernate.Session;

public class StateMain {
	public static void main(String[] args) {
		
		// Transient State
		Tourist tourist = new Tourist();
		tourist.setName("Adam Gilchrist");
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// Pesisitence state
		session.save(tourist);
		//hibernate sens the changes change of object which is in persistence state and fire a optimized query to database.
		tourist.setName("Barak Obama");
		tourist.setName("Balamidir Puttine");
		session.getTransaction().commit();
		session.close();
		
		//Detached State: no update query to database as it is now detached object
		tourist.setName("Mruga Kumar");

		HibernateUtil.shutdown();
	}
}
