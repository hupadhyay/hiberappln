package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Address;
import in.himtech.persist.dto.AnotherUser;

import org.hibernate.Session;

public class AnotherUserMain {
	public static void main(String[] args) {
		Address add1 = new Address();
		add1.setCity("Lucknow");
		add1.setPincode("226008");
		add1.setState("UP");
		add1.setStreet("Sarojini Nagar");
		
		Address offAdd1 = new Address();
		offAdd1.setCity("Noida");
		offAdd1.setPincode("201301");
		offAdd1.setState("UP");
		offAdd1.setStreet("Sector-62");

		AnotherUser antrUser = new AnotherUser();
		antrUser.setUserName("Hritika");
		antrUser.setAddress(add1);
		antrUser.setOfficeAddress(offAdd1);

		Address add2 = new Address();
		add2.setCity("Ghaziabad");
		add2.setPincode("201012");
		add2.setState("UP");
		add2.setStreet("Sarojini Nagar");
		
		Address offAdd2 = new Address();
		offAdd2.setCity("Gurgaon");
		offAdd2.setPincode("110034");
		offAdd2.setState("Haryana");
		offAdd2.setStreet("Sector-21");

		AnotherUser antrUser1 = new AnotherUser();
		antrUser1.setUserName("Hrishik");
		antrUser1.setAddress(add2);
		antrUser1.setOfficeAddress(offAdd2);

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(antrUser);
		session.save(antrUser1);
		session.getTransaction().commit();
		session.close();
		HibernateUtil.shutdown();
	}
}
