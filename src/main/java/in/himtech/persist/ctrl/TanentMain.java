package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Address;
import in.himtech.persist.dto.Tanent;

import org.hibernate.Session;

public class TanentMain {
	public static void main(String[] args) {

		Address add1 = new Address();
		add1.setCity("Lucknow");
		add1.setPincode("226008");
		add1.setState("UP");
		add1.setStreet("Sarojini Nagar");
		
		Address offAdd1 = new Address();
		offAdd1.setCity("Noida");
		offAdd1.setPincode("201301");
		offAdd1.setState("UP");
		offAdd1.setStreet("Sector-62");
		
		Address add2 = new Address();
		add2.setCity("Ghaziabad");
		add2.setPincode("201012");
		add2.setState("UP");
		add2.setStreet("Sarojini Nagar");
		
		Address offAdd2 = new Address();
		offAdd2.setCity("Gurgaon");
		offAdd2.setPincode("110034");
		offAdd2.setState("Haryana");
		offAdd2.setStreet("Sector-21");
		
		Tanent tanent = new Tanent();
		tanent.setName("Himanshu");
		tanent.getListAddress().add(offAdd2);
		tanent.getListAddress().add(offAdd1);
		tanent.getListAddress().add(add2);
		tanent.getListAddress().add(add1);
		
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(tanent);
		session.getTransaction().commit();
		session.close();
		
		
		Session session1 = HibernateUtil.getSessionFactory().openSession();
		Tanent tanent1 = (Tanent)session1.load(Tanent.class, 1);
		System.out.println(tanent.getName() + ", ID: " + tanent1.getId());
		session1.close();
		System.out.println("Num of Address: " + tanent.getListAddress().size());
		
		HibernateUtil.shutdown();
	
	}
}
