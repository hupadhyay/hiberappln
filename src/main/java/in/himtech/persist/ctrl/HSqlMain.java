package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Tourist;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

public class HSqlMain {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Query query = session.createQuery("from Tourist where tourId > ? and name = ?");
		query.setInteger(0, 5);
		query.setString(1, "Videshi 9");
		List<Tourist> listTourist = query.list();
		
		
		SQLQuery sqlQuery = session.createSQLQuery("select * from Tourist").addEntity(Tourist.class);
		List<Tourist> listTourist1 = sqlQuery.list();
		
		session.getTransaction().commit();
		session.close();
		
		System.out.println("Number of Touriest is: " + listTourist.size());
		System.out.println(listTourist);
		System.out.println(listTourist1);
		HibernateUtil.shutdown();
	}
}
