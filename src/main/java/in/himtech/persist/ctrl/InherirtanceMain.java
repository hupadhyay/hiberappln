package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Circle;
import in.himtech.persist.dto.Rectangle;
import in.himtech.persist.dto.Shape;

import org.hibernate.Session;

public class InherirtanceMain {
	public static void main(String[] args) {
		Shape shape1 = new Shape();
		shape1.setName("shape");
		
		Circle circle = new Circle();
		circle.setName("circle");
		circle.setRadius(10.5);
		
		Rectangle rec = new Rectangle();
		rec.setName("rectangle");
		rec.setLength(12.4);
		rec.setWidth(3.4);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		session.save(shape1);
		session.save(circle);
		session.save(rec);
		
		session.getTransaction().commit();
		session.close();

		HibernateUtil.shutdown();
	}
}
