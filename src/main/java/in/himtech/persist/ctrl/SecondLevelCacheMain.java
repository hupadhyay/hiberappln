package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Tourist;

import org.hibernate.Session;

public class SecondLevelCacheMain {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Tourist tourist = (Tourist)session.get(Tourist.class, 1);
		tourist.setName("Bhartiya Yatri");
		
		session.getTransaction().commit();
		session.close();
		
		Session session1 = HibernateUtil.getSessionFactory().openSession();
		session1.beginTransaction();
		
		Tourist tourist1 = (Tourist)session1.get(Tourist.class, 1);
		System.out.println(tourist1);
		
		session1.getTransaction().commit();
		session1.close();
		
		HibernateUtil.shutdown();
	}
}
