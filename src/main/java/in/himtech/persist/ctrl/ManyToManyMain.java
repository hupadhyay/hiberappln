package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Consumer;
import in.himtech.persist.dto.Product;

import org.hibernate.Session;

public class ManyToManyMain {
	public static void main(String[] args) {
		Consumer con1 = new Consumer();
		con1.setName("Hritika");
		
		Consumer con2 = new Consumer();
		con2.setName("Hrishik");
		
		Product prod1 = new Product();
		prod1.setName("Doll");
		
		Product prod2 = new Product();
		prod2.setName("Toy");
		
		con1.getListProduct().add(prod2);
		con1.getListProduct().add(prod1);
		
		con2.getListProduct().add(prod1);
		
		prod1.getListConsumer().add(con2);
		prod2.getListConsumer().add(con1);
		prod2.getListConsumer().add(con2);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(prod1);
		session.save(prod2);
		session.save(con1);
		session.save(con2);
		session.getTransaction().commit();
		session.close();

		HibernateUtil.shutdown();
	}
}
