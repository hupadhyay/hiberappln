package in.himtech.persist.ctrl;

import in.himtech.persist.dto.Tourist;

import org.hibernate.Session;

public class CrudMain {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		// C --> CREATE
		for(int i=0; i < 10; i++){
			Tourist tourist = new Tourist();
			tourist.setName("Videshi " + (i+1));
			session.save(tourist);
		}
		
		// R --> READ
		Tourist t1 = (Tourist) session.get(Tourist.class, 5);
		System.out.println(t1);
		
		// U --> Update
		t1.setName("My Indian tourist.");
		
		//D --> Delete
		Tourist t2 = (Tourist) session.get(Tourist.class, 6);
		session.delete(t2);
		
		session.getTransaction().commit();
		session.close();

		HibernateUtil.shutdown();
	}
}
