package in.himtech.persist.ctrl;

import java.util.List;

import in.himtech.persist.dto.Tourist;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class CriteriaQueryMain {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Tourist.class);
		criteria.add(Restrictions.like("name", "%Videshi%")).add(Restrictions.gt("tourId", 5));
		// by default it perform and operation to perform or operation use or
		// method
//		criteria.add(Restrictions.or(Restrictions.between("tourId", 0, 3),
//				Restrictions.between("tourId", 8, 10)));

		List<Tourist> listTourist = (List<Tourist>)criteria.list();

		System.out.println("Criteria Restrictions: " + listTourist);
		
		Criteria crtr = session.createCriteria(Tourist.class);
		crtr.setProjection(Projections.property("name"));
		
		List<String> strList = crtr.list();
		System.out.println("Criteria Projection: " + strList);
		
		Criteria crtr1 = session.createCriteria(Tourist.class);
		crtr1.setProjection(Projections.max("tourId"));
		Object obj  = crtr1.uniqueResult();
		System.out.println("Criteiral Aggreegaton Result:" + obj);

		session.getTransaction().commit();
		session.close();
		HibernateUtil.shutdown();
	}
}
