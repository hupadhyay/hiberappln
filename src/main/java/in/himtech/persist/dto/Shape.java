package in.himtech.persist.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
//@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
/*@DiscriminatorColumn(name="SHAPE_TYPE", discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue(value="SHAPE")*/
public class Shape {
	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer Id;
	private String Name;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

}
