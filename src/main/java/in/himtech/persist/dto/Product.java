package in.himtech.persist.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer prodId;
	private String name;
	@ManyToMany(mappedBy="listProduct")
	private List<Consumer> listConsumer = new ArrayList<Consumer>();

	public Integer getProdId() {
		return prodId;
	}

	public void setProdId(Integer prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Consumer> getListConsumer() {
		return listConsumer;
	}

	public void setListConsumer(List<Consumer> listConsumer) {
		this.listConsumer = listConsumer;
	}

}
