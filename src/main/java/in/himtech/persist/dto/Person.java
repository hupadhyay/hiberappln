package in.himtech.persist.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Embeddable
public class Person implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7853587333994184851L;

	@Id 
	@GeneratedValue
	private int id;
	
	@Column(name="AADHAR")
	private String adharNumber;
	
	@Column(name="PANCARD")
	private String pancard;
	
	@Column(name="P_NAME")
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdharNumber() {
		return adharNumber;
	}

	public void setAdharNumber(String adharNumber) {
		this.adharNumber = adharNumber;
	}

	public String getPancard() {
		return pancard;
	}

	public void setPancard(String pancard) {
		this.pancard = pancard;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", adharNumber=" + adharNumber
				+ ", pancard=" + pancard + ", name=" + name + "]";
	}
}
