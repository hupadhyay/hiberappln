package in.himtech.persist.dto;

import javax.persistence.Entity;

@Entity
/*@DiscriminatorValue(value="CIRCLE")*/
public class Circle extends Shape{
	private double radius;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

}
