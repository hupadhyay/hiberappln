package in.himtech.persist.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "BOOK")
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String name;
	
	@OneToMany(mappedBy="book")
/*	@JoinTable(name="BOOK_CPTR", joinColumns=@JoinColumn(name="BK_ID"), 
			inverseJoinColumns=@JoinColumn(name="CPTR_INDEX"))   */
	private List<Chapter> listChapter = new ArrayList<Chapter>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Chapter> getListChapter() {
		return listChapter;
	}

	public void setListChapter(List<Chapter> listChapter) {
		this.listChapter = listChapter;
	}

}
