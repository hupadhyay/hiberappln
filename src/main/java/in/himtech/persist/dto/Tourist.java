package in.himtech.persist.dto;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
@NamedQueries({@NamedQuery(name="touriest.all", query="from Tourist"),
		@NamedQuery(name="tourist.byid", query="from Tourist where tourId > ?")})
@NamedNativeQuery(name="tourist.native.byname", query="select * from Tourist", resultClass=Tourist.class)
public class Tourist {
	@Id
	@GeneratedValue
	private Integer tourId;

	private String name;

	public Integer getTourId() {
		return tourId;
	}

	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Tourist [tourId=" + tourId + ", name=" + name + "]";
	}
}
