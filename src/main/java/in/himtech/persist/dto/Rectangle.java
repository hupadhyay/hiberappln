package in.himtech.persist.dto;

import javax.persistence.Entity;

@Entity
/*@DiscriminatorValue(value="RECTANGLE")*/
public class Rectangle extends Shape{
	private double length;
	private double width;

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

}
